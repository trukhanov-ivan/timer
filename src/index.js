'use strict';
import '../style.css';
import { Timer } from './timer.js';

const inputBlock = document.querySelector('.input');
const titleDateValue = document.querySelector('#title-date');
const selectDateInput = document.querySelector('#date');
const buttonStart = document.querySelector('#btn');
const buttonReset = document.querySelector('#btn-reset');
const userTitle = document.querySelector('h1');
const countdownScreen = document.querySelector('.output');
const blockTimer = document.querySelector('.numbers');
const startTitleValue = 'Создать новый таймер обратного отсчета';
let timer;

function startTimer() {
    timer = new Timer(selectDateInput.value, blockTimer);

    if (!timer.init()) {
        return;
    }

    this.deadlineDate = moment(selectDateInput.value);
    this.titleValue = titleDateValue.value;

    localStorage.setItem('deadlineDate', this.deadlineDate);
    localStorage.setItem('titleValue', this.titleValue);

    countdownScreenReplace();
}

function countdownScreenReplace() {
    countdownScreen.classList.toggle('hide');
    buttonReset.classList.toggle('hide');
    inputBlock.classList.toggle('hide');
    buttonStart.classList.toggle('hide');

    userTitle.textContent = titleDateValue.value;
}

function restoreCountdoun() {
    const dateTimer = localStorage.getItem('deadlineDate');
    const titleTimer = localStorage.getItem('titleValue');

    if (!dateTimer) {
        return;
    }

    timer = new Timer(dateTimer, blockTimer);

    if (!timer.init()) {
        return;
    }

    titleDateValue.value = titleTimer;

    countdownScreenReplace();
}

function resetTimer() {
    countdownScreenReplace();
    userTitle.textContent = startTitleValue;
    selectDateInput.value = '';
    titleDateValue.value = '';

    clearInterval(timer.timerInterval);
    localStorage.removeItem('deadlineDate');
    localStorage.removeItem('titleValue');
}

buttonStart.addEventListener('click', startTimer);
buttonReset.addEventListener('click', resetTimer);

restoreCountdoun();
