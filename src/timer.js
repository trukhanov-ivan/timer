export class Timer {
    constructor(deadlineDate, blockTimer) {
        this.deadlineDate = deadlineDate;
        this.timerInterval = null;
        this.blockTimer = blockTimer;
    }

    init() {
        if (this.deadlineDate === '') {
            alert('Пожалуйста введите дату');
            return false;
        }

        this.countTimer();
        this.timerInterval = setInterval(this.countTimer.bind(this), 1000);
        return true;
    }

    countTimer() {
        let dateNow = moment();

        if (moment(this.deadlineDate) <= dateNow) {
            clearInterval(this.timerInterval);
            this.blockTimer.textContent = `Таймер завершился ${this.deadlineDate} `;
            return;
        }

        const deadlineDateMoment = moment(this.deadlineDate);
        const seconds = deadlineDateMoment.diff(dateNow, 'seconds') % 60;
        const mins = deadlineDateMoment.diff(dateNow, 'minutes') % 60;
        const hours = deadlineDateMoment.diff(dateNow, 'hours') % 60;
        const days = deadlineDateMoment.diff(dateNow, 'days') % 24;
        this.blockTimer.textContent = `${days}:${hours}:${mins}:${seconds}`;
    }
}
